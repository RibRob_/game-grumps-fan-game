using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;


public class UIControl : MonoBehaviour
{
    public GameObject EscapeMenu;

    public static bool isPaused = false;

    public void Escape(InputAction.CallbackContext context)
    {
        Debug.Log("TEST ESCAPE");
        Debug.Log(isPaused);
        Debug.Log(context);
        
        foreach (var prop in context.GetType().GetProperties())
        {
            Debug.Log(prop.Name + "=" + prop.GetValue(context, null));
        }

        Debug.Log(context.GetType().GetProperties());
        if (context.canceled)
        {
            
        
        if (isPaused)
        {
            Resume();
        }
        else
        {
            Pause();
        }
        }
    }
    public void Resume()
    {
        EscapeMenu.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;

    }
    void Pause()
    {
        EscapeMenu.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;

    }

    public void QuitConfirm()
    {
        Debug.Log("Are you Sure you want to quit?");
    }

    public void QuitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
