//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPickUpable {
    
    void Use();
}
