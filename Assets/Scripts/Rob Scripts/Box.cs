//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour , IDamageable<float> {

    public int current_health = 3;
    public AudioSource audio_source;
    public AudioClip damage_sfx;
    public GameSettings game_settings;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Damage(float amount) {
        //Debug.Log("I've been damaged! My health was at " + current_health);
        current_health -= (int)amount;
        //Debug.Log("Now my health is at " + current_health);
        //Play sound
        CheckHealth();
    }

    private void CheckHealth() {
        if (current_health <= 0) {
            Destroy(this.gameObject);
        }
    }
}
