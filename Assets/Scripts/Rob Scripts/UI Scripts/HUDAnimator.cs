//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDAnimator : MonoBehaviour {

    [Header("Elements to Animate")]
    [SerializeField] private RectTransform _dan_health_bar;
    [SerializeField] private RectTransform _arin_health_bar;
    [SerializeField] private RectTransform _fp_bar;
    [SerializeField] private RectTransform _coin_counter;
    [SerializeField] private RectTransform _location_text;
    [SerializeField] private GameObject _alignment_grid_object;

    [Space][Header("Player Information")]
    [SerializeField] private Transform _dan_transform;
    [SerializeField] private Vector3 _dans_last_location = new Vector3();
    [SerializeField] private Transform _arin_transform;
    [SerializeField] private Vector3 _arins_last_location = new Vector3();

    [Space][Header("Variables")]
    [SerializeField] private float _slide_speed = 3f;
    [SerializeField] private float _idle_length = 0f;
    [SerializeField] private float _hud_delay = 4f;
    [SerializeField] private float _display_delay = 2f;
    [SerializeField] private AnimationCurve _ease_curve;

    [Space][Header("Buffers")]
    [SerializeField] private float _bar_buffer = 178f;
    [SerializeField] private float _fp_buffer = 69f;
    [SerializeField] private float _coin_buffer = 84f;
    [SerializeField] private float _location_buffer = 261f;
    [SerializeField] private float _out_buffer = -261f;
    
    [Space][Header("Display Bools")]
    [SerializeField] private bool _idle_display = true;
    [SerializeField] private bool _dans_healthbar_displayed = false;
    [SerializeField] private bool _arins_healthbar_displayed = false;
    [SerializeField] private bool _fp_bar_displayed = false;
    [SerializeField] private bool _coins_displayed = false;
    [SerializeField] private bool _location_text_displayed = false;

    private Coroutine _last_dan_health_bar_coroutine;

    // Start is called before the first frame update
    void Start() {
        _dans_last_location = _dan_transform.position;
        _arins_last_location = _arin_transform.position;
    }
    
    // Update is called once per frame
    void Update() {
        CheckPlayerMovement();
    }

    private void CheckPlayerMovement() {
        if (_dans_last_location == _dan_transform.position && _arins_last_location == _arin_transform.position) {
            _idle_length += Time.deltaTime;
        }
        else {
            _idle_length = 0;

            if (_idle_display) {
                _idle_display = false;
                DanHealthBarSlideOut();
                ArinHealthBarSlideOut();
                FPBarSlideOut();
                CoinCounterSlideOut();
                LocationTextSlideOut();
            }
        }

        if (_idle_length >= _hud_delay && !_idle_display) {
            _idle_display = true;
            DanHealthBarSlideIn();
            ArinHealthBarSlideIn();
            FPBarSlideIn();
            CoinCounterSlideIn();
            LocationTextSlideIn();
        }

        _dans_last_location = _dan_transform.position;
        _arins_last_location = _arin_transform.position;
    }

    private IEnumerator DisplayDelay() {
        if (!_idle_display) { 
            yield return new WaitForSeconds(_display_delay);

            if (_dans_healthbar_displayed) {
                DanHealthBarSlideOut();
            }

            if (_arins_healthbar_displayed) {
                ArinHealthBarSlideOut();
            }

            if (_fp_bar_displayed) {
                FPBarSlideOut();
            }

            if (_coins_displayed) {
                CoinCounterSlideOut();
            }

            if (_location_text_displayed) {
                LocationTextSlideOut();
            }
        }
    }

    public void DanHealthBarSlideIn() {
        LeanTween.moveX(_dan_health_bar, _bar_buffer, _slide_speed).setEase(_ease_curve);
        _dans_healthbar_displayed = true;
        StartCoroutine("DisplayDelay");
    }

    public void DanHealthBarSlideOut() {
        LeanTween.moveX(_dan_health_bar, _out_buffer + _bar_buffer, _slide_speed).setEase(_ease_curve);
        _dans_healthbar_displayed = false;
    }

    public void ArinHealthBarSlideIn() {
        LeanTween.moveX(_arin_health_bar, _bar_buffer, _slide_speed).setEase(_ease_curve);
        _arins_healthbar_displayed = true;
        StartCoroutine("DisplayDelay");
    }

    public void ArinHealthBarSlideOut() {
        LeanTween.moveX(_arin_health_bar, _out_buffer + _bar_buffer, _slide_speed).setEase(_ease_curve);
        _arins_healthbar_displayed = false;
    }

    public void FPBarSlideIn() {
        LeanTween.moveX(_fp_bar, _fp_buffer, _slide_speed).setEase(_ease_curve);
        _fp_bar_displayed = true;
        StartCoroutine("DisplayDelay");
    }

    public void FPBarSlideOut() {
        LeanTween.moveX(_fp_bar, _out_buffer + _fp_buffer, _slide_speed).setEase(_ease_curve);
        _fp_bar_displayed = false;
    }

    public void CoinCounterSlideIn() {
        LeanTween.moveX(_coin_counter, -_coin_buffer, _slide_speed).setEase(_ease_curve);
        _coins_displayed = true;
        StartCoroutine("DisplayDelay");
    }

    public void CoinCounterSlideOut() {
        LeanTween.moveX(_coin_counter, -_out_buffer - _coin_buffer, _slide_speed).setEase(_ease_curve);
        _coins_displayed = false;
    }

    public void LocationTextSlideIn() {
        LeanTween.moveX(_location_text, _location_buffer, _slide_speed).setEase(_ease_curve);
        _location_text_displayed = true;
        StartCoroutine("DisplayDelay");
    }

    public void LocationTextSlideOut() {
        LeanTween.moveX(_location_text, _out_buffer + _location_buffer, _slide_speed).setEase(_ease_curve);
        _location_text_displayed = false;
    }

    public void ToggleAlignmentGrid() {
        _alignment_grid_object.SetActive(_alignment_grid_object.activeInHierarchy);
    }
}
