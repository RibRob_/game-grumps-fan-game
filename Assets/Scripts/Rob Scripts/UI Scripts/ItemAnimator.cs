//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemAnimator : MonoBehaviour {

    [SerializeField] private GameObject _item_prompt_prefab;
    [SerializeField] private Transform _item_prompt_parent;
    [SerializeField] private float _slide_time = 1f;
    [SerializeField] private RectTransform _dummy_item;
    [SerializeField] private List<RectTransform> _item_prompts = new List<RectTransform>();

    public void NewItem(string n, Sprite s) {
        GameObject new_item = GameObject.Instantiate(_item_prompt_prefab, _item_prompt_parent);
        RectTransform new_item_rect_transform = new_item.GetComponent<RectTransform>();
        new_item.GetComponent<UIItemPrompt>().SetItem(n, s);
        new_item_rect_transform.localPosition.Set(_dummy_item.localPosition.x + 130f, _dummy_item.localPosition.y, _dummy_item.localPosition.z);
        LeanTween.moveX(new_item_rect_transform, _dummy_item.localPosition.x - 10f, _slide_time);
        MakeRoomForNewItemPromptsDown(new_item_rect_transform);
    }

    private void MakeRoomForNewItemPromptsDown(RectTransform new_rt) {
        foreach (RectTransform rt in _item_prompts) {
            if (rt != null) {
                LeanTween.moveY(rt, rt.localPosition.y + 60f, _slide_time);
            }
        }

        _item_prompts.Insert(0, new_rt);
        new_rt.GetComponent<UIItemPrompt>().DelayedFade();
    }
}
