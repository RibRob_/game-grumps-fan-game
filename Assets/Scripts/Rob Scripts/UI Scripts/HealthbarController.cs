//Created by Ribrob
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthbarController : MonoBehaviour {

    [Header("Variables")]
    [SerializeField] private PlayerInformation player_info;
    [SerializeField] private float speed = 1f;

    [Space][Header("UI Elements")]
    [SerializeField] private Slider dan_healthbar;
    [SerializeField] private TextMeshProUGUI dan_health_text;
    [SerializeField] private Slider arin_healthbar;
    [SerializeField] private TextMeshProUGUI arin_health_text;

    private void Awake() {
        dan_healthbar.value = player_info.current_health_dan / player_info.max_health_dan;
        dan_health_text.text = player_info.current_health_dan + "/" + player_info.max_health_dan;

        arin_healthbar.value = player_info.current_health_arin / player_info.max_health_arin;
        arin_health_text.text = player_info.current_health_arin + "/" + player_info.max_health_arin;
    }

    // Update is called once per frame
    void Update() {
        DanHealth();
        ArinHealth();
    }

    private void DanHealth() {
        if (dan_healthbar.value - (player_info.current_health_dan / player_info.max_health_dan) > 0.005f) {
            dan_healthbar.value -= Time.deltaTime * speed;
        }
        else if (dan_healthbar.value - (player_info.current_health_dan / player_info.max_health_dan) < -0.005f) {
            dan_healthbar.value += Time.deltaTime * speed;
        }
    }

    private void ArinHealth() {
        if (arin_healthbar.value - (player_info.current_health_arin / player_info.max_health_arin) > 0.005f) {
            arin_healthbar.value -= Time.deltaTime * speed;
        }
        else if (arin_healthbar.value - (player_info.current_health_arin / player_info.max_health_arin) < -0.005f) {
            arin_healthbar.value += Time.deltaTime * speed;
        }
    }
}
