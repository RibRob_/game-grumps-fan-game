//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CoinCounterController : MonoBehaviour {

    [SerializeField] private PlayerInformation _player_info;
    [SerializeField] private TextMeshProUGUI _coin_counter_text;
    [SerializeField] private HUDAnimator _hud_animator;

// Start is called before the first frame update
    void Start() {
        UpdateCounterNumber(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateCounterNumber(bool slide) { 
        if (_player_info.money < 10) {
            _coin_counter_text.text = "000";
        }
        else if (_player_info.money > 10 && _player_info.money < 100) {
            _coin_counter_text.text = "00";
        }
        else if (_player_info.money > 100 && _player_info.money < 1000) {
            _coin_counter_text.text = "0";
        }
        else {
            _coin_counter_text.text = "";
        }

        _coin_counter_text.text += _player_info.money.ToString();

        if (slide) {
            _hud_animator.CoinCounterSlideIn();
        }
    }
    
}
