//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIItemPrompt : MonoBehaviour {

    [SerializeField] private TextMeshProUGUI _item_name_text;
    [SerializeField] private Image _item_image;
    [SerializeField] private CanvasGroup _own_canvas_group;
    [SerializeField] private float _time_until_fade = 1f;
    [SerializeField] private float _time_to_fade = 4f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() {
        if (_own_canvas_group.alpha <= 0) {
            Destroy(this.gameObject);
        }
    }

    public void SetItem(string n, Sprite s) {
        _item_name_text.text = n;
        _item_image.sprite = s;
    }

    public void DelayedFade() {
        StartCoroutine("Fade");
    }

    private IEnumerator Fade() {
        yield return new WaitForSeconds(_time_until_fade);
        LeanTween.alphaCanvas(_own_canvas_group, 0, _time_to_fade);
    }
}
