//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour {

    [SerializeField] private string _level_name;
    [SerializeField] private string _spawn_point_name;
    [SerializeField] private string _walk_point_name;
    [SerializeField] private PlayerInformation player_information;
    [SerializeField] public bool can_enter_level = true;
    [SerializeField] public int num_players_in_trigger = 0;

    private void OnTriggerEnter(Collider other) {
        Debug.Log(other.name + " entered the level loader!");
        if (other.tag == "Player") {
            num_players_in_trigger++;

            if (can_enter_level) {
                other.gameObject.SetActive(false);
                if (other.name == "Dan") {
                    GameObject arin_go = GameObject.Find("Arin");
                    arin_go.SetActive(false);
                }
                else if (other.name == "Arin") {
                    GameObject dan_go = GameObject.Find("Dan");
                    dan_go.SetActive(false);
                }

                player_information.spawn_point_name = _spawn_point_name;
                player_information.walk_point_name = _walk_point_name;
                SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
                SceneManager.LoadScene(_level_name, LoadSceneMode.Additive);
            }
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.tag == "Player") {
            num_players_in_trigger--;

            if (num_players_in_trigger <= 0) {
                can_enter_level = true;
            }
        }
    }
}
