//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnLoadManager : MonoBehaviour
{

    [SerializeField] private string _level_name;
    [SerializeField] private PlayerInformation _player_information;
    [SerializeField] private GameObject _dan_go;
    [SerializeField] private UnoccupiedAI _dan_ai;
    [SerializeField] private GameObject _arin_go;
    [SerializeField] private UnoccupiedAI _arin_ai;
    [SerializeField] private CharacterManager _character_manager;
    [SerializeField] private UnoccupiedAI _first_player_AI;
    [SerializeField] private bool _walking = false;
    [SerializeField] private float _delay_before_walking = 1f;
    [SerializeField] private bool _sequence_complete = true;
    [SerializeField] private Transform _walk_point;

    private void Start()
    {
        GameObject player_start_object = GameObject.Find("Player Start");

        if (player_start_object == null) {
            _sequence_complete = false;

            //Set as the active scene
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(_level_name));
            Debug.Log("Set active scene");

            //find Dan and Arin
            Debug.Log("Searching for Dan and Arin");
            _character_manager = GameObject.Find("Character Manager").GetComponent<CharacterManager>();
            _dan_go = _character_manager.dan_go;
            _arin_go = _character_manager.arin_go;

            //Set Dan and Arin to active
            _dan_go.SetActive(true);
            _arin_go.SetActive(true);
            Debug.Log("Set Dan and Arin to active");

            Debug.Log("About to find spawn points");
            //Find spawn point
            if (_player_information.spawn_point_name != null || _player_information.spawn_point_name != "")
            {
                Transform spawn_point = GameObject.Find(_player_information.spawn_point_name).transform;
                _walk_point = GameObject.Find(_player_information.walk_point_name).transform;
                Debug.Log("Found spawn and walk points");

                //Move players to spawn point
                _dan_go.transform.position = spawn_point.position;
                _arin_go.transform.position = spawn_point.position;

                //have dan and arin walk to spawn point
                _character_manager.MakeBothPlayersAIs(out _first_player_AI);
                StartCoroutine(GoToWalkPoint());
            }
        }
        else
        {
            Debug.Log("Player Start is present in scene");
        }
    }

    IEnumerator GoToWalkPoint() {
        _sequence_complete = false;
        yield return new WaitForSeconds(_delay_before_walking);
        _first_player_AI.TargetToFollow = _walk_point;
        _walking = true;
        Debug.Log("Going to walk point");

    }

    // Update is called once per frame
    void Update() {
        if (_first_player_AI != null) {
            if (Mathf.Abs(_first_player_AI.RemainingDistance) <= 0.1f && !_sequence_complete) {
                _sequence_complete = true;
                _character_manager.ReturnPlayerControl();
                Debug.Log("Returned player controls");
            }
        }
    }
}
