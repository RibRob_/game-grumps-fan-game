//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpable : MonoBehaviour, IPickUpable{

    [SerializeField] private GameSettings _game_settings;
    [SerializeField] private AudioSource _throw_audio_source;
    [SerializeField] private AudioClip _throw_clip;
    public Item item = null;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Use() { 
    }

    public void PutAway() {
    }

    public void PlayThrowSound() {
        if (_throw_clip != null) {
            _throw_audio_source.volume = _game_settings.sfx_volume;
            _throw_audio_source.clip = _throw_clip;
            _throw_audio_source.Play();
        }
    }
}
