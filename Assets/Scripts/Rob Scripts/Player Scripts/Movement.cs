//Created by RibRob
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

public enum MovementState { 
    Idling,
    Walking,
    Sprinting,
    Jumping,
}
public class Movement : MonoBehaviour {

    //Input variables
    private OverworldActions overworldActions;
    private InputAction _walk_input;
    private InputAction.CallbackContext _sprint_context;

    [Header("Speeds")]
    [SerializeField] private float _walk_speed = 2f;
    [SerializeField] private float _sprint_speed = 4f;
    [SerializeField] private float _current_speed = 2f;
    [SerializeField] private float _animated_speed = 0f;

    [Space] [Header("Jump Variables")]
    [SerializeField] private float _jump_force = 5f;
    [SerializeField] private float _fall_multiplier = 2.5f;
    [SerializeField] private bool _grounded = true;
    [SerializeField] private bool _has_landed = true;
    [SerializeField] private PhysicMaterial _jumping_material;
    [SerializeField] private Collider _player_collider;
    [SerializeField] private Rigidbody _player_rigidbody;
    public ParticleSystem sprint_particles;
    public float grounded_skin = 0.1f;
    public LayerMask ground_mask;
    public Vector3 box_center;
    public Vector3 box_size = new Vector3(0.5f, 0.5f, 0.5f);
    private Collider[] _colliders;
    public Collider player_collider;
    public float buffer_distance = 0.25f;

    [Space] [Header("SFX")]
    [SerializeField] private AudioSource _jump_audio_source;
    [SerializeField] private List<AudioClip> _jump_sounds;
    [SerializeField] private AudioSource _foot_step_audio_source;
    [SerializeField] private AudioClip _walk_clip;
    [SerializeField] private AudioClip _sprint_clip;
    [SerializeField] private AudioSource _land_audio_source;

    [Space] [Header("Booleans")]
    [SerializeField] private bool _can_walk = true;
    [SerializeField] private bool _can_sprint = true;
    [SerializeField] private bool _can_jump = true;
    [SerializeField] private bool _being_carried = false;

    [Space][Header("Movement Info")]
    [SerializeField] private float _lr_movement;
    [SerializeField] private float _fb_movement;

    [Space][Header("Animations")]
    [SerializeField] private Animator _sprite_animator;

    //Uncategorized
    [Space]
    [SerializeField] public Vector2 _raw_movement;
    [SerializeField] private int _player_index = 0;
    [SerializeField] private SpriteRenderer _player_sprite;
    [SerializeField] private GameSettings _game_settings;
    [SerializeField] private UnoccupiedAI _unoccupied_ai;
    [SerializeField] private MovementState _current_state = MovementState.Idling;
    [SerializeField] public PlayerInput player_input;

    [Space][Header("Events")]
    public UnityEvent<PlayerInput> Landed = new UnityEvent<PlayerInput>();


    //Accessors
    public float CurrentSpeed {
        get { return _current_speed; }
    }

    public float AnimatedSpeed {
        get { return _animated_speed; }
    }

    public bool CanWalk {
        get { return _can_walk; }
        set { _can_walk = value; }
    }

    public bool CanSprint {
        get { return _can_sprint; }
        set { _can_sprint = value; }
    }

    public bool CanJump {
        get { return _can_jump; }
        set { _can_jump = value; }
    }

    public bool BeingCarried {
        get { return _being_carried; }
        set { _being_carried = value; }
    }

    public float LeftRightMovement {
        get { return _lr_movement; }
    }

    public float ForwardBackwardMovement {
        get { return _fb_movement; }
    }

    public bool Grounded {
        get { return _grounded; }
    }

    public MovementState CurrentState {
        get { return _current_state; }
    }

    public int PlayerIndex {
        get { return _player_index; }
    }

    private void Awake() {
        overworldActions = new OverworldActions();
    }

    private void OnEnable() {
        //Definitely need these
        _walk_input = overworldActions.Player.Walk;
        _walk_input.Enable();
    }

    private void OnDisable() {
        _walk_input = overworldActions.Player.Walk;
        overworldActions.Player.Walk.Disable();
    }

    // Start is called before the first frame update
    void Start() {
        box_size = new Vector3(player_collider.transform.localScale.x - buffer_distance, grounded_skin, player_collider.transform.localScale.z - buffer_distance);
        sprint_particles.Stop();
    }

    //Update using the physics engine
    private void FixedUpdate() {
        Walk();
        ApplyExtraGravity();
        CheckSprintParticles();
        CheckIfLanded();
    }

    public void SetMovement(InputAction.CallbackContext context) {
        _raw_movement = context.ReadValue<Vector2>();
    }

    private void Walk() {
        //This method will only execute code if the player can walk and the raw movement is greater than 0
        //This method is in fixed update because it needs to be checked continuously
        if (_can_walk && !_being_carried) {

            _animated_speed = _raw_movement.magnitude * _current_speed;
            _sprite_animator.SetFloat("Speed", _animated_speed);

            if (_raw_movement.y >= 0) {
                _sprite_animator.SetBool("Front", false);
            }
            else if (_raw_movement.y < 0) {
                _sprite_animator.SetBool("Front", true);
            }

            if (_raw_movement.magnitude > 0) {
                _lr_movement = _raw_movement.x;
                _fb_movement = _raw_movement.y;

                if (_current_speed > _walk_speed && _grounded) {
                    _current_state = MovementState.Sprinting;
                }
                else if (_current_speed < _sprint_speed && _grounded) {
                    _current_state = MovementState.Walking;
                }

                //Flip sprite when moving to left or right
                if (_lr_movement > 0) {
                    _player_sprite.flipX = true;
                }
                else if (_lr_movement < 0) {
                    _player_sprite.flipX = false;
                }

                Vector3 total_movment = new Vector3(_lr_movement, 0, _fb_movement);
                total_movment.Normalize();

                this.transform.Translate(total_movment * _current_speed * Time.deltaTime);

                _foot_step_audio_source.volume = (_game_settings.sfx_volume / 100) / 2;
                if (!_foot_step_audio_source.isPlaying && _grounded) {
                    _foot_step_audio_source.Play();
                }
                else if (_foot_step_audio_source.isPlaying && !_grounded) {
                    _foot_step_audio_source.Stop();
                }
            }
            else {
                _foot_step_audio_source.Stop();

                if (_grounded) {
                    _current_state = MovementState.Idling;
                }
            }
        }
    }
 

    public void Sprint(InputAction.CallbackContext context) {
        _sprint_context = context;

        if (_can_sprint && !_being_carried) {
            if (context.started) {
                _current_speed = _sprint_speed;
                _foot_step_audio_source.clip = _sprint_clip;
            }
            else if (context.canceled) {
                _current_speed = _walk_speed;
                sprint_particles.Stop();
                _foot_step_audio_source.clip = _walk_clip;
            }

            //If the sprint button is being held and the particles aren't already playing, play them
            if ((context.started || context.performed) && !sprint_particles.isPlaying) {
                sprint_particles.Play();
            }
        }
    }

    private void CheckSprintParticles() {
        if (!_grounded) {
            sprint_particles.Stop();
        }

        if (_walk_input.ReadValue<Vector2>().magnitude <= 0 && sprint_particles.isPlaying) {
            sprint_particles.Stop();
        }
    }

    public void Jump(InputAction.CallbackContext context) {
        if (_can_jump && !_being_carried) {
            if (context.started && _grounded && _has_landed) {
                _player_collider.material = _jumping_material;
                _player_rigidbody.velocity = Vector3.up * _jump_force;
                _grounded = false;
                _has_landed = false;
                PlayJumpSound();
                _foot_step_audio_source.Stop();
                _current_state = MovementState.Jumping;
                _sprite_animator.SetBool("Jumping", true);

                if (_unoccupied_ai.enabled) {
                    _unoccupied_ai.StartDelayedJump();
                }
            }
        }
    }

    private void ApplyExtraGravity() {
        //Make the player fall faster if at the peak of their jump height
        if (_player_rigidbody.velocity.y <= 0) {
            _player_rigidbody.velocity += Vector3.up * Physics.gravity.y * (_fall_multiplier - 1) * Time.deltaTime;
        }
    }

    private void CheckIfLanded() {
        if (!_has_landed) {
            //Calculate the center of the box to be cast
            box_center = (Vector3)transform.position + Vector3.down * (player_collider.transform.localScale.y + grounded_skin) * 0.5f;
            box_center.y += 0.45f;
            //Get all colliders in box
            _colliders = Physics.OverlapBox(box_center, box_size / 4, this.transform.rotation);

            //If there are no colliders at all, then the foreach loop will not run.
            //so if the array is empty, then the player is not touching ANY colliders
            //and is therefore not grounded.
            if (_colliders.Length == 0) {
                _player_collider.material = _jumping_material;
                _grounded = false;
            }

            //Check if any are walkable. If so, you're grounded.
            foreach (Collider c in _colliders) {

                //If grounded was false earlier, and you're holding the
                //Sprint button, then turn on the sprint particles.
                if (_grounded == false && (_sprint_context.started || _sprint_context.performed)) {
                    sprint_particles.Play();
                }

                _grounded = true;
                break;
            }
        }
    }

    private void PlayJumpSound() {
        int index = UnityEngine.Random.Range(0, _jump_sounds.Count);
        _jump_audio_source.clip = _jump_sounds[index];
        _jump_audio_source.volume = (_game_settings.sfx_volume / 100);
        _jump_audio_source.Play();
    }

    public void SetMovement(bool b) {
        _can_walk = b;
        _can_sprint = b;
        _can_jump = b;
    }

    private void OnCollisionEnter(Collision collision) {
        if (_grounded && !_has_landed) {
            _has_landed = true;

            Landed.Invoke(player_input);
            _sprite_animator.SetBool("Jumping", false);
            player_collider.material = null;

            _player_rigidbody.velocity = new Vector3(0, 0, 0); //Prevents sliding

            if (!_land_audio_source.isPlaying) {
                _land_audio_source.volume = (_game_settings.sfx_volume / 100) / 2;
                _land_audio_source.Play();
            }
        }

    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireCube(box_center, box_size);
    }
}
