//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Rumbler : MonoBehaviour {

    private Gamepad _gamepad;
    public bool rumble_enabled = true;

    [Header("Bash Rumble Options")]
    [SerializeField] private float _bash_rumble_duration = 0.5f;
    [SerializeField] private float _bash_high_frequency = 0.5f;
    [SerializeField] private float _bash_low_frquency = 0.5f;

    [Header("Land Rumble Options")]
    [SerializeField] private float _land_rumble_duration = 0.2f;
    [SerializeField] private float _land_high_frequency = 0.7f;
    [SerializeField] private float _land_low_frquency = 0.0f;

    public IEnumerator BashRumble(PlayerInput player_input) {
        if (rumble_enabled) {
            _gamepad = GetGamePad(player_input);

            if (_gamepad != null) {
                _gamepad.SetMotorSpeeds(_bash_low_frquency, _bash_high_frequency);
                yield return new WaitForSeconds(_bash_rumble_duration);
                _gamepad.SetMotorSpeeds(0f, 0f);
            }
        }
    }
    
    public IEnumerator LandRumble(PlayerInput player_input) {
        if (rumble_enabled) {
            _gamepad = GetGamePad(player_input);

            if (_gamepad != null) {
                _gamepad.SetMotorSpeeds(_land_low_frquency, _land_high_frequency);
                yield return new WaitForSeconds(_land_rumble_duration);
                _gamepad.SetMotorSpeeds(0f, 0f);
            }
        }
    }

    public void PlayLandRumble(PlayerInput pi) {
        StartCoroutine(LandRumble(pi));
    }

    public void PlayBashRumble(PlayerInput pi) {
        StartCoroutine(BashRumble(pi));
    }


    private Gamepad GetGamePad(PlayerInput player_input) {
        if (player_input != null) {
            Debug.Log("Player Input Name: " + player_input.name);

            foreach (InputDevice id in player_input.devices) {
                if (id.displayName == "Xbox Controller") {
                    return (Gamepad)id;
                }
            }
        }
        else {
            Debug.Log("Player input is null");
        }
        return null;
    }
}
