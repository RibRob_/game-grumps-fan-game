//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerInputHandler : MonoBehaviour {

    [Header("Player details")]
    public string character_name = "";
    public PlayerInput player_input;
    public int player_index;
    public bool device_lost = false;

    [Space][Header("Player Scripts")]
    public Movement movement;
    public Respawn respawn;
    public LookProjector look_projector;
    public Interact interact;
    public DebugController debug_controller;
    public Bash bash;
    public UnoccupiedAI unoccupied_ai;
    public CharacterManager character_manager;

    [Space][Header("Input Info")]
    public OverworldActions overworldActions;

    private void Awake() {
        player_input = GetComponent<PlayerInput>();
        player_index = player_input.playerIndex;

        overworldActions = new OverworldActions();
    }

    // Start is called before the first frame update
    void Start() {
        SceneManager.MoveGameObjectToScene(this.gameObject, SceneManager.GetSceneByName("Player Scene"));
    }

    // Update is called once per frame
    void Update() {
        
    }

    public void OnMove(InputAction.CallbackContext context) {
        if (movement != null) {
            movement.SetMovement(context);
            look_projector.UpdateMovement(context);
        }
    }

    public void OnJump(InputAction.CallbackContext context) {
        if (movement != null) {
            movement.Jump(context);
        }
    }

    public void OnSprint(InputAction.CallbackContext context) {
        if (movement != null) {
            movement.Sprint(context);
        }
    }

    public void OnBash(InputAction.CallbackContext context) {
        if (interact != null && bash != null) {
            interact.PutAway(context);
            bash.DoBash(context);
        }
    }

    public void OnInteract(InputAction.CallbackContext context) {
        if (interact != null) {
            interact.DoInteraction(context);
        }
    }

    public void OnCheat(InputAction.CallbackContext context) {
        if (debug_controller != null) {
            debug_controller.ToggleDebug(context);
        }
        else {
            Debug.Log("debug controller is null when trying to open cheats");
        }
    }

    public void OnReturn(InputAction.CallbackContext context) {
        if (debug_controller != null) {
            debug_controller.ToggleDebug(context);
        }
        else {
            Debug.Log("debug controller is null when returning");
        }
    }

    public void OnUse(InputAction.CallbackContext context) {
        interact.UseHeldItem(context);
    }

    public void OnSwitchCharacters(InputAction.CallbackContext context) {
        if (character_manager != null) {
            character_manager.SwapCharacters(context);
        }
    }

    public void DeviceLost(PlayerInput player_input) {
        device_lost = true;
        movement.enabled = false;
        look_projector.enabled = false;
        interact.enabled = false;
        debug_controller.enabled = false;
        bash.enabled = false;
        unoccupied_ai.enabled = true;
        character_manager.DeviceLost(this);
    }

    public void DeviceRegained(PlayerInput player_input) {
        device_lost = false;
        movement.enabled = true;
        look_projector.enabled = true;
        interact.enabled = true;
        debug_controller.enabled = true;
        bash.enabled = true;
        unoccupied_ai.enabled = false;
        character_manager.DeviceRegained();
    }

}
