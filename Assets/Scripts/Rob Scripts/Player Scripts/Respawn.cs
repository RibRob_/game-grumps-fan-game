//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour {

    [Header("Depth and Buffer")]
    [SerializeField] private float _max_depth = -20f;
    [SerializeField] private float _buffer_y = 2f;

    [Space][Header("Last Grounded Info")]
    [SerializeField] private Vector3 _last_grounded_movement_direction;
    [SerializeField] private Vector3 _last_grounded_location;

    [Space][Header("Componenets")]
    [SerializeField] private Transform _player_transform;
    [SerializeField] private Movement _player_movement;
    [SerializeField] private Rigidbody _player_rigidbody;

    private void Awake() {
        _player_transform = GetComponent<Transform>();
        _player_movement = GetComponent<Movement>();
        _player_rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update() {
        if (_player_movement.Grounded && (_player_movement.LeftRightMovement != 0 || _player_movement.ForwardBackwardMovement != 0)) {
            _last_grounded_location = _player_transform.position;
            _last_grounded_movement_direction = new Vector3(_player_movement.LeftRightMovement, 0, _player_movement.ForwardBackwardMovement);
        }
        if (_player_transform.position.y <= _max_depth) {
            _player_rigidbody.velocity = Vector3.zero;
            Vector3 target = new Vector3(_last_grounded_location.x + (-1.5f * _last_grounded_movement_direction.x), _last_grounded_location.y + _buffer_y, _last_grounded_location.z + (-1.5f * _last_grounded_movement_direction.z));
            _player_transform.position = target;
        }
    }
}
