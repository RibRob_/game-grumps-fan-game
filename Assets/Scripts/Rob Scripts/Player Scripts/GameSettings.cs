//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Game Settings", menuName = "ScriptableObjects/GameSettingsObject")]
public class GameSettings : ScriptableObject {

    public float music_volume = 100f;
    public float sfx_volume = 100f;
}
