//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class LookProjector : MonoBehaviour {

    [Header("Transforms")]
    public Transform look_axis_transform;
    public Transform projector_origin_transform;
    public Transform player_transform;

    [Space][Header("Misc")]
    private OverworldActions _overworldActions;
    private InputAction _walk_input;
    [SerializeField] private bool _can_look = true;
    [SerializeField] Vector2 _raw_movement;

    public bool CanLook {
        get { return _can_look; }
    }

    private void Awake() {
        _overworldActions = new OverworldActions();
    }

    private void OnEnable() {
        _walk_input = _overworldActions.Player.Walk;
        _walk_input.Enable();
    }

    void FixedUpdate() {
        RotateLookAxis();
    }

    public void UpdateMovement(InputAction.CallbackContext context) {
        _raw_movement = context.ReadValue<Vector2>();
    }

    private void RotateLookAxis() {
        Vector3 rotate_value = new Vector3(-_raw_movement.y, 0, _raw_movement.x);

        if (_raw_movement.magnitude > 0) {
            //Rotate the bash axis using the left stick input and the player rotation
            look_axis_transform.rotation = Quaternion.LookRotation(rotate_value) * player_transform.rotation;
        }
    }
}
