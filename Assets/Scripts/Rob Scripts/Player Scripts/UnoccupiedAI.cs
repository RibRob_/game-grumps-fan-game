//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnoccupiedAI : MonoBehaviour {

    [Header("Speeds")]
    [SerializeField] private float _current_speed = 3f;

    [Space][Header("Distances")]
    [SerializeField] private float _follow_distance = 1.5f;
    [SerializeField] private float _current_distance = 0f;
    [SerializeField] private float _remaining_distance = 0f;
    [SerializeField] private float _y_seperation = 0f;

    [Space][Header("Booleans")]
    [SerializeField] private bool _can_walk = true;
    [SerializeField] private bool _can_jump = true;

    [Space][Header("Jump Info")]
    [SerializeField] private float _jump_delay = 0.2f;
    [SerializeField] private float _jump_force = 10f;
    [SerializeField] private float _fall_multiplier = 2.5f;
    [SerializeField] private PhysicMaterial _jumping_material;
    [SerializeField] private bool _jumping = false;

    [Space][Header("SFX")]
    [SerializeField] private GameSettings _game_settings;
    [SerializeField] private AudioSource _jump_audio_source;
    [SerializeField] private List<AudioClip> _jump_sounds;
    [SerializeField] private AudioSource _foot_step_audio_source;
    [SerializeField] private AudioClip _walk_clip;
    [SerializeField] private AudioClip _sprint_clip;
    [SerializeField] private AudioSource _land_audio_source;

    [Space][Header("Target Info")]
    [SerializeField] private Movement _targeted_movement;
    [SerializeField] private Transform _target_to_follow;

    [Space][Header("AI Info")]
    [SerializeField] private Rigidbody _ai_rigidbody;
    [SerializeField] private Collider _ai_collider;
    [SerializeField] private SpriteRenderer _ai_sprite_renderer;
    [SerializeField] public Animator ai_animator;
    [SerializeField] private Vector3 _direction;

    public Transform TargetToFollow {
        get { return _target_to_follow; }
        set { _target_to_follow = value; }
    }

    public float RemainingDistance { 
        get { return _remaining_distance; }
    }

    private void FixedUpdate() {
        Walk();
        ApplyExtraGravity();
        CheckY();
        FacePlayer();
        MatchSpeed();
        UpdateAnimatorSpeed();
    }
            
    private void Walk() {
        if (_can_walk) {

            if (_target_to_follow != null) {
                Vector3 targeted_position = _target_to_follow.position;
                _current_distance = Vector3.Distance(transform.position, _target_to_follow.position);

                _remaining_distance = _current_distance - _follow_distance;
                if (_current_distance > _follow_distance) {
                    transform.Translate(-_direction * Time.deltaTime * _current_speed * (_remaining_distance * 2));
                }
            }
        }
    }

    private void CheckY() {
        _y_seperation = _target_to_follow.position.y - transform.position.y;
        if (_y_seperation < -0.4f) {
            Jump();
        }
    }

    private void FacePlayer() {
        _direction = (transform.position - _target_to_follow.position).normalized;

        if (_direction.x > 0) {
            _ai_sprite_renderer.flipX = false;
        }
        else if (_direction.x < 0) {
            _ai_sprite_renderer.flipX = true;
        }

        if (_direction.z > 0) {
            ai_animator.SetBool("Front", true);
        }
        else if (_direction.z < 0) {
            ai_animator.SetBool("Front", false);
        }

    }

    private void MatchSpeed() {
        if (_current_speed != _targeted_movement.CurrentSpeed) {
            _current_speed = _targeted_movement.CurrentSpeed;
        }
    }

    private void Jump() {
        if (_can_jump && !_jumping) {
            _ai_collider.material = _jumping_material;
            _jumping = true;
            _ai_rigidbody.velocity = Vector3.up * _jump_force;
            PlayJumpSound();
            _foot_step_audio_source.Stop();
            ai_animator.SetBool("Jumping", true);
        }
    }

    public void StartDelayedJump() {
        StartCoroutine(DelayedJump());
    }

    private IEnumerator DelayedJump() {
        if (_can_jump && !_jumping) {
            yield return new WaitForSeconds(_jump_delay);
            Jump();
        }
    }

    private void ApplyExtraGravity() {
        //Make the player fall faster if at the peak of their jump height
        if (_ai_rigidbody.velocity.y <= 0) {
            _ai_rigidbody.velocity += Vector3.up * Physics.gravity.y * (_fall_multiplier - 1) * Time.deltaTime;
        }
    }

    private void UpdateAnimatorSpeed() {
        if ((_targeted_movement.CurrentState == MovementState.Idling || _current_distance <= _follow_distance) && _remaining_distance <= 0.01f) {
            ai_animator.SetFloat("Speed", 0f);
        }
        else {
            ai_animator.SetFloat("Speed", _current_speed);
        }
    }


    private void PlayJumpSound() {
        int index = UnityEngine.Random.Range(0, _jump_sounds.Count);
        _jump_audio_source.clip = _jump_sounds[index];
        _jump_audio_source.volume = (_game_settings.sfx_volume / 100);
        _jump_audio_source.Play();
    }

    private void OnCollisionEnter(Collision collision) {
        if (_jumping) {
            ai_animator.SetBool("Jumping", false);
            _jumping = false;
            _ai_collider.material = null;
        }

        if (Mathf.Abs(_y_seperation) > 0.3 && !_jumping && _y_seperation < 0.96f) {
            Jump();
        }
    }

}
