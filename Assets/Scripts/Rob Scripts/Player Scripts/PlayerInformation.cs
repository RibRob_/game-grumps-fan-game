//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Player Info", menuName = "ScriptableObject/PlayerInfoObjects", order = 2)]
public class PlayerInformation : ScriptableObject {

    public float current_health_arin = 10f;
    public float max_health_arin = 10f;
    public float current_health_dan = 10f;
    public float max_health_dan = 10f;
    public float current_fp = 10f;
    public float max_fp = 10;
    public int money = 0;
    public int good_boi_coins = 0;
    public int max_sticker_points = 2;
    public int used_sticker_points = 0;
    public Item[] items = new Item[10];
    public string spawn_point_name = "";
    public string walk_point_name = "";
    //inventory
    //stickers

    public string current_level = "";
    public string previous_level = "";
    public bool already_loaded = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DamageDan(float damage) {
        current_health_dan -= damage;
    }

    public void DamageArin(float damage) {
        current_health_arin -= damage;
    }

    public void ClearItems() {
        for (int i = 0; i < items.Length; i++) {
            items[i] = null;
        }
    }

    public void AddMoney(int amount) {
        money += amount;
    }

    public void HealDan() {
        current_health_dan = max_health_dan;
    }

    public void HealArin() {
        current_health_arin = max_health_arin;
    }

    public void HealBoth() {
        HealDan();
        HealArin();
    }

    public void RefillFP() {
        current_fp = max_fp;
    }

    public void FullRefresh() {
        HealBoth();
        RefillFP();
    }
}
