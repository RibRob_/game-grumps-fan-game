//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CollisionDetector : MonoBehaviour {

    [SerializeField] private PlayerInformation _player_info;
    [SerializeField] private CoinCounterController _coin_counter_controller;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.collider.tag == "Coin") {
            Destroy(collision.gameObject);
            _player_info.AddMoney(1);
            _coin_counter_controller.UpdateCounterNumber(true);
            //Play sound
        }
    }
}
