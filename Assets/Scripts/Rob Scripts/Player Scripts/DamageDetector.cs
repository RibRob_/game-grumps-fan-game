//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DamageDetector : MonoBehaviour {
    [SerializeField] private string character_name = "Dan";
    [SerializeField] private PlayerInformation player_info;
    [SerializeField] private HealthbarController healthbar_controller;
    [SerializeField] private float damage_cool_down = 2f;
    [SerializeField] private bool can_take_collision_damage = true;
    [SerializeField] private HUDAnimator _hud_animator;
    [SerializeField] private TextMeshProUGUI _health_text;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.collider.tag == "Damageable") {
            can_take_collision_damage = false;
            StartCoroutine("DamageCoolDown");
            //Animations or special effects
            //Sounds

            if (character_name == "Dan") {
                _hud_animator.DanHealthBarSlideIn();
                player_info.DamageDan(1f);
                _health_text.text = player_info.current_health_dan + "/" + player_info.max_health_dan;
            }
            else if (character_name == "Arin") {
                _hud_animator.ArinHealthBarSlideIn();
                player_info.DamageArin(1f);
                _health_text.text = player_info.current_health_arin + "/" + player_info.max_health_arin;
            }
        }
    }

    private IEnumerator DamageCoolDown() {
        yield return new WaitForSeconds(damage_cool_down);
        can_take_collision_damage = true;
    }
}
