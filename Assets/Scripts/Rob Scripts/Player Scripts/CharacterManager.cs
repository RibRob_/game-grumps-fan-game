//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;

public enum FirstplayerState { 
    Dan,
    Arin
}

public class CharacterManager : MonoBehaviour {

    [Header("General Info")]
    public PlayerInputManager player_input_manager;
    public bool multiplayer = false;
    public int num_players_joined = 0;

    [Space][Header("Cool Down Info")]
    public float cool_down_amount = 1f;
    public bool cool_down_complete = true;

    [Space][Header("Camera Info")]
    public CinemachineVirtualCamera follow_camera;
    public CinemachineTargetGroup group_follow_camera;

    [Space][Header("First Player Info")]
    public PlayerInput first_player = null;
    public PlayerInputHandler first_player_playerinputhandler;
    public string first_player_selected_character = "Dan";
    public FirstplayerState first_player_state = FirstplayerState.Dan;

    [Space][Header("Second Player Info")]
    public PlayerInputHandler second_player_playerinputhandler;
    public PlayerInput second_player = null;
    public string second_player_selected_character = "Arin";

    [Space][Header("Dan Stuff")]
    public GameObject dan_go;
    public List<MonoBehaviour> dan_player_components = new List<MonoBehaviour>();
    public Movement dan_movement;
    public Respawn dan_respawn;
    public Interact dan_interact;
    public LookProjector dan_lookprojector;
    public DebugController dan_debugcontroller;
    public PlayerInput dan_playerinput;
    public Bash dan_bash;
    public UnoccupiedAI dan_unoccupied_ai;
    public Transform dan_transform;

    [Space][Header("Arin Stuff")]
    public GameObject arin_go;
    public List<MonoBehaviour> arin_player_components = new List<MonoBehaviour>();
    public Movement arin_movement;
    public Respawn arin_respawn;
    public Interact arin_interact;
    public LookProjector arin_lookprojector;
    public DebugController arin_debugcontroller;
    public PlayerInput arin_playerinput;
    public Bash arin_bash;
    public UnoccupiedAI arin_unoccupied_ai;
    public Transform arin_transform;

    private void Awake() {
        player_input_manager = this.GetComponent<PlayerInputManager>();
        
        //Add Dan's componenets
        dan_player_components.Add(dan_movement);
        dan_player_components.Add(dan_interact);
        dan_player_components.Add(dan_lookprojector);
        dan_player_components.Add(dan_debugcontroller);
        dan_player_components.Add(dan_bash);

        //Add Arin's Componenets
        arin_player_components.Add(arin_movement);
        arin_player_components.Add(arin_interact);
        arin_player_components.Add(arin_lookprojector);
        arin_player_components.Add(arin_debugcontroller);
        arin_player_components.Add(arin_bash);
    }


    public void SwapCharacters(InputAction.CallbackContext context) {
        if (context.started && cool_down_complete && !multiplayer) {
            StartCoroutine(CoolDown());

            //Swap control from Arin to Dan
            if (first_player_state == FirstplayerState.Arin) {
                first_player_state = FirstplayerState.Dan;

                DisablePlayerInputOnCharacter(arin_unoccupied_ai, arin_player_components);
                EnablePlayerInputOnCharacter(dan_unoccupied_ai, dan_player_components);

                if (!first_player_playerinputhandler.device_lost) {
                    SetPlayerInputHandlerToDan(first_player_playerinputhandler);

                    if (second_player_playerinputhandler != null) {
                        SetPlayerInputHandlerToArin(second_player_playerinputhandler);
                    }
                }
                else if (second_player_playerinputhandler != null) {
                    if (!second_player_playerinputhandler.device_lost) {
                        SetPlayerInputHandlerToDan(second_player_playerinputhandler);
                        SetPlayerInputHandlerToArin(first_player_playerinputhandler);
                    }
                }

                follow_camera.Follow = dan_transform;
            }
            //Swap control from Dan to Arin
            else if (first_player_state == FirstplayerState.Dan) {
                first_player_state = FirstplayerState.Arin;

                DisablePlayerInputOnCharacter(dan_unoccupied_ai, dan_player_components);
                EnablePlayerInputOnCharacter(arin_unoccupied_ai, arin_player_components);

                if (!first_player_playerinputhandler.device_lost) {
                    SetPlayerInputHandlerToArin(first_player_playerinputhandler);

                    if (second_player_playerinputhandler != null) {
                        SetPlayerInputHandlerToDan(second_player_playerinputhandler);
                    }
                }
                else if (second_player_playerinputhandler != null) {
                    if (!second_player_playerinputhandler.device_lost) {
                        SetPlayerInputHandlerToArin(second_player_playerinputhandler);
                        SetPlayerInputHandlerToDan(first_player_playerinputhandler);
                    }
                }

                follow_camera.Follow = arin_transform;
            }
        }

    }


    private IEnumerator CoolDown() {
        cool_down_complete = false;
        yield return new WaitForSeconds(cool_down_amount);
        cool_down_complete = true;
    }

    public void PlayerJoined(PlayerInput player_input) {
        //Set up Dan
        if (num_players_joined == 0 && first_player_state == FirstplayerState.Dan) {
            first_player = player_input;
            dan_playerinput = player_input;
            first_player_playerinputhandler = player_input.GetComponent<PlayerInputHandler>();
            SetPlayerInputHandlerToDan(first_player_playerinputhandler);
            dan_bash.player_input = player_input;
            dan_movement.player_input = player_input;
            first_player_playerinputhandler.character_manager = this.GetComponent<CharacterManager>();
        }
        else if (num_players_joined == 0 && first_player_state == FirstplayerState.Arin) {
            first_player = player_input;
            arin_playerinput = player_input;
            first_player_playerinputhandler = player_input.GetComponent<PlayerInputHandler>();
            SetPlayerInputHandlerToArin(first_player_playerinputhandler);
            arin_bash.player_input = player_input;
            arin_movement.player_input = player_input;
            first_player_playerinputhandler.character_manager = this.GetComponent<CharacterManager>();
        }
        else if (num_players_joined == 1) {
            second_player = player_input;
            second_player_playerinputhandler = player_input.GetComponent<PlayerInputHandler>();

            if (first_player_state == FirstplayerState.Dan) {
                arin_playerinput = player_input;
                SetPlayerInputHandlerToArin(second_player_playerinputhandler);
                arin_bash.player_input = player_input;
                arin_movement.player_input = player_input;
                EnablePlayerInputOnCharacter(arin_unoccupied_ai, arin_player_components);
            }
            else if (first_player_state == FirstplayerState.Arin) {
                dan_playerinput = player_input;
                SetPlayerInputHandlerToDan(second_player_playerinputhandler);
                dan_bash.player_input = player_input;
                dan_movement.player_input = player_input;
                EnablePlayerInputOnCharacter(dan_unoccupied_ai, dan_player_components);
            }

            second_player_playerinputhandler = player_input.gameObject.GetComponent<PlayerInputHandler>();
            second_player_playerinputhandler.character_manager = this.GetComponent<CharacterManager>();

            dan_interact.Multiplayer = true;
            arin_interact.Multiplayer = true;

            multiplayer = true;

            follow_camera.Follow = group_follow_camera.transform;
        }
        num_players_joined++;
    }

    private void EnablePlayerInputOnCharacter(UnoccupiedAI ai, List<MonoBehaviour> player_components) {
        ai.enabled = false;
        foreach (MonoBehaviour mb in player_components) {
            mb.enabled = true;
        }
    }

    private void DisablePlayerInputOnCharacter(UnoccupiedAI ai, List<MonoBehaviour> player_components) {
        foreach (MonoBehaviour mb in player_components) {
            mb.enabled = false;
        }
        ai.enabled = true;
    }

    private void SetPlayerInputHandlerToDan(PlayerInputHandler player_input_handler) {
        player_input_handler.name = "Dan Input Handler";
        player_input_handler.movement = dan_movement;
        player_input_handler.respawn = dan_respawn;
        player_input_handler.look_projector = dan_lookprojector;
        player_input_handler.interact = dan_interact;
        player_input_handler.debug_controller = dan_debugcontroller;
        player_input_handler.bash = dan_bash;
        dan_bash.player_input = player_input_handler.player_input;
        dan_movement.player_input = player_input_handler.player_input;
        player_input_handler.unoccupied_ai = dan_unoccupied_ai;
    }

    private void SetPlayerInputHandlerToArin(PlayerInputHandler player_input_handler) {
        player_input_handler.name = "Arin Input Handler";
        player_input_handler.movement = arin_movement;
        player_input_handler.respawn = arin_respawn;
        player_input_handler.look_projector = arin_lookprojector;
        player_input_handler.interact = arin_interact;
        player_input_handler.debug_controller = arin_debugcontroller;
        player_input_handler.bash = arin_bash;
        arin_bash.player_input = player_input_handler.player_input;
        arin_movement.player_input = player_input_handler.player_input;
        player_input_handler.unoccupied_ai = arin_unoccupied_ai;
    }

    public void DeviceLost(PlayerInputHandler player_input_handler) { 
        if (multiplayer || second_player_playerinputhandler != null) {

            if (player_input_handler.name == "Dan") {
                follow_camera.Follow = arin_transform;
            }
            else if (player_input_handler.name == "Arin") {
                follow_camera.Follow = dan_transform;
            }
        }

        multiplayer = false;
        dan_interact.Multiplayer = false;
        arin_interact.Multiplayer = false;
    }

    public void DeviceRegained() {
        if (second_player_playerinputhandler != null) {
            multiplayer = true;
            dan_interact.Multiplayer = true;
            arin_interact.Multiplayer = true;
        }

        if (multiplayer || second_player_playerinputhandler != null) {
            follow_camera.Follow = group_follow_camera.transform;
        }
    }

    public void MakeBothPlayersAIs(out UnoccupiedAI first_player_AI) {
        DisablePlayerInputOnCharacter(dan_unoccupied_ai, dan_player_components);
        DisablePlayerInputOnCharacter(arin_unoccupied_ai, arin_player_components);

        first_player_AI = null;

        if (first_player_state == FirstplayerState.Dan) {
            first_player_AI = dan_unoccupied_ai;
            //dan_unoccupied_ai.TargetToFollow = target;
            arin_unoccupied_ai.TargetToFollow = dan_transform;
        }
        else if (first_player_state == FirstplayerState.Arin) {
            first_player_AI = arin_unoccupied_ai;
            //arin_unoccupied_ai.TargetToFollow = target;
            dan_unoccupied_ai.TargetToFollow = arin_transform;
        }
    }

    public void ReturnPlayerControl() {
        if (first_player_state == FirstplayerState.Dan) {
            EnablePlayerInputOnCharacter(dan_unoccupied_ai, dan_player_components);

            if (second_player != null) {
                EnablePlayerInputOnCharacter(arin_unoccupied_ai, arin_player_components);
            }
        }
        else if (first_player_state == FirstplayerState.Arin) {
            EnablePlayerInputOnCharacter(arin_unoccupied_ai, arin_player_components);

            if (second_player != null) {
                EnablePlayerInputOnCharacter(dan_unoccupied_ai, dan_player_components);
            }
        }
    }
}
