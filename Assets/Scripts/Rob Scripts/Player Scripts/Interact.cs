//Created by RibRob
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class Interact : MonoBehaviour {

    [Header("Input Variables")]
    private OverworldActions overworldActions;
    private InputAction _walk_input;
    private InputAction.CallbackContext _throw_context;

    [Space][Header("Values")]
    [SerializeField] private float _current_throw_force = 40f;
    [SerializeField] private float _item_throw_force = 40f;
    [SerializeField] private float _player_throw_force = 400f;
    [SerializeField] private float _cool_down = 1f;
    [SerializeField] private bool _multiplayer = false;

    [Space][Header("Booleans")]
    [SerializeField] private bool _can_interact = true;
    [SerializeField] private bool _can_pick_up = true;
    [SerializeField] private bool _can_throw = true;
    [SerializeField] private bool _can_place = true;
    [SerializeField] private bool _can_put_away = true;
    [SerializeField] private bool _can_use = true;
    [SerializeField] private bool _holding_something = false;
    [SerializeField] private bool _cool_down_complete = true;
    [SerializeField] private bool _talking = false;

    [Space][Header("Detection")]
    private Collider[] _colliders;
    public LayerMask interactable_mask;
    public Vector3 box_size = new Vector3(0.5f, 0.5f, 0.5f);

    [Space][Header("Object Interaction Management")]
    [SerializeField] private IInteractable _selected_interactable;
    [SerializeField] private IPickUpable _selected_pickupable;
    [SerializeField] private GameObject _selected_gameobject;
    [SerializeField] private GameObject _held_object;
    [SerializeField] private Rigidbody _held_object_rigidbody;

    [Space][Header("Transforms")]
    [SerializeField] private Transform _interact_location;
    [SerializeField] private Transform _hold_point;

    [Space][Header("SFXs")]
    [SerializeField] private AudioSource _interaction_audio_source;
    [SerializeField] private AudioSource _throw_audio_source;
    [SerializeField] private AudioClip _pick_up_clip;
    [SerializeField] private AudioClip _drop_clip;
    [SerializeField] private AudioClip _put_away_clip;
    [SerializeField] private AudioClip _throw_clip;
    [SerializeField] private GameSettings _game_settings;

    [Space][Header("Movement Info")]
    [SerializeField] private Vector2 _last_raw_movement;

    [Space][Header("Animation")]
    [SerializeField] private Animator _sprite_animator;
    [SerializeField] private ItemAnimator _item_animator;

    [Space][Header("Misc")]
    [SerializeField] private Bash _player_bash;
    [SerializeField] private PlayerInformation _player_information;
    [SerializeField] private PlayerCharacter _own_player_character;
    [SerializeField] private Movement _player_movement;
    [SerializeField] private GameObject _marker;

    public bool Multiplayer {
        get { return _multiplayer; }
        set { _multiplayer = value; }
    }

    public bool CanPutAway {
        get { return _can_put_away; }
    }

    public bool CanThrow {
        get { return _can_throw; }
    }

    public bool Talking {
        get { return _talking; }
        set { _talking = value; }
    }

    public bool CanPlace {
        get { return _can_place; }
    }

    public bool CanPickUp {
        get { return _can_pick_up; }
    }

    public bool HoldingSomething {
        get { return _holding_something; }
    }

    private void Awake() {
        overworldActions = new OverworldActions();
    }

    private void OnEnable() {
        _walk_input = overworldActions.Player.Walk;
        _walk_input.Enable();
    }

    void FixedUpdate() {
        if (_can_interact) {
            CheckObject();
        }
    }

    private void CheckObject() {

        //This only occurs if the player is moving.
        if (_player_movement.CurrentState == MovementState.Walking) {

            _last_raw_movement = _player_movement._raw_movement;

            //Get colliders that are interactable
            _colliders = Physics.OverlapBox(_interact_location.position, box_size / 2, Quaternion.identity, interactable_mask);
            //GameObject thing = Instantiate(_marker, _interact_location.position, Quaternion.identity);
            //Check if there are any colliders at all
            if (_colliders.Length <= 0) {
                _selected_interactable = null;
                _selected_pickupable = null;
                _selected_gameobject = null;
            }

            foreach (Collider c in _colliders) {
                //Debug.Log("In collider array: " + c.name);
                //Check if collider implements IInteractable or IPickUpable. If they are, select the first one
                IInteractable i = c.GetComponent<IInteractable>();
                IPickUpable p = c.GetComponent<IPickUpable>();
                PlayerCharacter pc = c.GetComponent<PlayerCharacter>();

                if (pc != _own_player_character) {
                    if (i != null && p != null) {

                        if (pc != null && !_multiplayer) {
                            //Debug.Log("Can talk to: " + c.name);
                            _selected_interactable = i;
                            _selected_pickupable = null;
                            _selected_gameobject = c.gameObject;
                            break;
                        }
                        else if (pc != null && _multiplayer) {
                            //Debug.Log("Can pick up: " + c.name);
                            _selected_pickupable = p;
                            _selected_interactable = null;
                            _selected_gameobject = c.gameObject;
                            break;
                        }

                    }
                    else if (i != null) {
                        _selected_interactable = i;
                        _selected_pickupable = null;
                        _selected_gameobject = c.gameObject;
                        break;
                    }
                    else if (p != null && !_holding_something) {
                        _selected_pickupable = p;
                        _selected_interactable = null;
                        _selected_gameobject = c.gameObject;
                        break;
                    }
                    else if (i == null && p == null) {
                        _selected_interactable = null;
                        _selected_pickupable = null;
                        _selected_gameobject = null;
                        Debug.Log("All null");
                        return;
                    }
                }
                else {
                    _selected_interactable = null;
                    _selected_pickupable = null;
                    _selected_gameobject = null;
                    //Debug.Log("Cannot interact with self");
                }
            }
        }
    }

    public void DoInteraction(InputAction.CallbackContext context) {
        if (_cool_down_complete) {

            if (context.started && _player_movement.CurrentState != MovementState.Idling && _holding_something) {
                Throw();
                _cool_down_complete = false;
                StartCoroutine(CoolDown());
            }
            else if (context.started && _holding_something) {
                Place();
                _cool_down_complete = false;
                StartCoroutine(CoolDown());
            }
            else if (context.started && _selected_interactable != null && (System.Object)_selected_interactable != (System.Object)_own_player_character) {
                _selected_interactable.Interact();
                _cool_down_complete = false;
                StartCoroutine(CoolDown());
            }
            else if (context.started && _selected_pickupable != null) {
                PickUp();
                _cool_down_complete = false;
                StartCoroutine(CoolDown());
            }
        }

    }

    private void PickUp() {
        if (_selected_pickupable is PlayerCharacter) {
            _current_throw_force = _player_throw_force;
            Movement other_players_movement = _selected_gameobject.GetComponent<Movement>();
            other_players_movement.BeingCarried = true;
            //Tell animator to play animation
        }

        _selected_pickupable = null;
        _holding_something = true;
        _held_object = _selected_gameobject;
        _selected_gameobject = null;
        _held_object.transform.position = _hold_point.transform.position;
        _held_object.transform.SetParent(_hold_point.transform);
        _held_object.GetComponent<Collider>().enabled = false;
        _held_object_rigidbody = _held_object.GetComponent<Rigidbody>();
        _held_object_rigidbody.isKinematic = true;
        _player_bash.CanBash = false;

        //Play sfx
        _interaction_audio_source.volume = _game_settings.sfx_volume / 100;
        _interaction_audio_source.clip = _pick_up_clip;
        _interaction_audio_source.Play();

        _sprite_animator.SetBool("HoldingSomething", true);
    }

    private void Place() {
        if (_selected_interactable == null) {
            Movement other_players_movement = _held_object.GetComponent<Movement>();
            if (other_players_movement != null) {
                other_players_movement.BeingCarried = false;
            }

            _holding_something = false;
            _held_object.transform.parent = null;
            _held_object_rigidbody.isKinematic = false;
            _held_object.GetComponent<Collider>().enabled = true;


            _held_object.transform.position = _interact_location.position;
            _selected_gameobject = _held_object;
            _selected_pickupable = _held_object.GetComponent<IPickUpable>();

            _held_object_rigidbody = null;
            _held_object = null;
            _player_bash.CanBash = true;
            _current_throw_force = _item_throw_force;

            //Play sfx
            _interaction_audio_source.volume = _game_settings.sfx_volume / 100;
            _interaction_audio_source.clip = _drop_clip;
            _interaction_audio_source.Play();

            _sprite_animator.SetBool("HoldingSomething", false);
        }
    }

    private void Throw() {
        Movement other_players_movement = _held_object.GetComponent<Movement>();
        if (other_players_movement != null) {
            other_players_movement.BeingCarried = false;
        }

        _holding_something = false;
        _held_object_rigidbody.isKinematic = false;
        _held_object.GetComponent<Collider>().enabled = true;

        _held_object.transform.parent = _interact_location;

        Vector3 raw_rot = new Vector3(_last_raw_movement.x, 0.4f, _last_raw_movement.y);
        raw_rot.Normalize();

        _held_object_rigidbody.AddForce(raw_rot * _current_throw_force);
        _held_object.transform.parent = null;

        _throw_audio_source.volume = _game_settings.sfx_volume / 100;
        _throw_audio_source.Play();

        _held_object_rigidbody = null;
        _held_object = null;
        _player_bash.CanBash = true;
        _current_throw_force = _item_throw_force;

        _sprite_animator.SetBool("HoldingSomething", false);
    }

    public void PutAway(InputAction.CallbackContext context) {

        Vector2 raw_movement = _walk_input.ReadValue<Vector2>();

        if (context.started && raw_movement.magnitude == 0 && _holding_something) {

            //Check if held object is an item
            Item item = _held_object.GetComponent<PickUpable>().item;
            if (item == null) {
                //Play negative sound
            }
            else {

                //Check if there are any empty slots
                bool success = false;

                for (int i = 0; i < _player_information.items.Length; i++) {
                    if (_player_information.items[i] == null) {
                        _player_information.items[i] = item;
                        success = true;

                        _item_animator.NewItem(item.name, item.sprite);

                        StartCoroutine(RenableBashCoolDown());

                        ResetHeldObjectInfo(true, false);

                        //Play sfx
                        _interaction_audio_source.volume = _game_settings.sfx_volume / 100;
                        _interaction_audio_source.clip = _put_away_clip;
                        _interaction_audio_source.Play();

                        //Animate
                        _sprite_animator.SetBool("HoldingSomething", false);
                        _current_throw_force = _item_throw_force;


                        //Break on success so it doesn't fill the whole array
                        break;
                    }
                }

                if (!success) {
                    //display no room in inventory message
                    Debug.Log("No more room in inventory");

                    //Play negative sound
                }
            }

        }

    }

    public void UseHeldItem(InputAction.CallbackContext context) {
        if (_can_use) {
            if (context.performed && _holding_something) {

                IPickUpable puable = _held_object.GetComponent<PickUpable>();
                puable.Use();

                ResetHeldObjectInfo(true, true);
                _current_throw_force = _item_throw_force;
            }
        }
    }

    private void ResetHeldObjectInfo(bool destroy, bool enable_bash) {
        if (destroy) {
            Destroy(_held_object);
        }

        _held_object = null;
        _held_object_rigidbody = null;
        _holding_something = false;
        _player_bash.CanBash = enable_bash;
        _selected_gameobject = null;
        _selected_pickupable = null;
    }

    IEnumerator RenableBashCoolDown() {
        yield return new WaitForSeconds(_cool_down);
        _player_bash.CanBash = true;
    }
    IEnumerator CoolDown() {
        yield return new WaitForSeconds(_cool_down);
        _cool_down_complete = true;
    }
}
