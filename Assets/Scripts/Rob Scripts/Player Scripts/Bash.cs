//Created by RibRob
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

public class Bash : MonoBehaviour {

    [Header ("Values")]
    [SerializeField] private float _damage = 1f;
    [SerializeField] private float _bash_force = 5f;

    [Space][Header("Transforms")]
    [SerializeField] private Transform _bash_transform;

    [Space][Header ("Detection")]
    public Vector3 box_size = new Vector3(0.5f, 0.5f, 0.5f);
    [SerializeField] private Interact _player_interact;
    private Collider[] _colliders;
    private List<IDamageable<float>> _damagables = new List<IDamageable<float>>();

    [Space][Header("Booleans")]
    [SerializeField] private bool _can_bash = true;
    [SerializeField] private bool _cool_down_complete = true;

    [Space][Header("Audio")]
    [SerializeField] private AudioSource _bash_audio_source;
    [SerializeField] private AudioClip _bash_audio_clip;
    [SerializeField] private GameSettings _game_settings;

    [Space][Header("Particle System")]
    [SerializeField] private ParticleSystem _bash_particle_system;

    [Space][Header("Animations")]
    [SerializeField] private Animator _sprite_animator;

    [Space][Header("Misc")]
    [SerializeField] private Movement _player_movement;
    [SerializeField] public PlayerInput player_input;

    [Space][Header("Events")]
    public UnityEvent<PlayerInput> BashedSomething = new UnityEvent<PlayerInput>();

    //Accessors
    public bool CanBash {
        get { return _can_bash; }
        set { _can_bash = value; }
    }

    public void DoBash(InputAction.CallbackContext context) {
        if (_can_bash) {
            if (context.started && !_player_interact.HoldingSomething && _cool_down_complete) {
                _cool_down_complete = false;
                _player_movement.SetMovement(false);

                _sprite_animator.SetBool("Bash", true);

                //Play bash sound
                _bash_audio_source.clip = _bash_audio_clip;
                _bash_audio_source.volume = _game_settings.sfx_volume / 100;
                _bash_audio_source.Play();

                //Make sure damageables are cleared from the list
                _damagables.Clear();

                //Check if any colliders are where the player is looking
                _colliders = Physics.OverlapBox(_bash_transform.position, box_size / 2, Quaternion.identity);
                if (_colliders.Length > 0) {

                    //Make sure it isn't just a trigger
                    bool more_than_just_triggers = false;

                    foreach (Collider c in _colliders) {
                        if (!c.isTrigger) {
                            more_than_just_triggers = true;
                        }
                    }

                    //If there's more than just triggers then do this stuff
                    if (more_than_just_triggers) {
                        _bash_particle_system.Play();
                        BashedSomething.Invoke(player_input);
                    }
                }

                foreach (Collider c in _colliders) {
                    Rigidbody rb = c.GetComponent<Rigidbody>();
                    if (rb != null) {
                        rb.AddExplosionForce(_bash_force, this.transform.position, 4f);
                    }

                    //Check if collider implements IDamageable. If they are, add to damageable list
                    IDamageable<float> id = c.GetComponent<IDamageable<float>>();
                    if (id != null) {
                        _damagables.Add(id);
                    }
                    else {
                        return;
                    }
                }

                //For each damageable that was detected, do damage
                foreach (IDamageable<float> id in _damagables) {
                    id.Damage(_damage);
                }

            }
        }
    }

    public void ResetBash() {
        _cool_down_complete = true;
        _player_movement.SetMovement(true);
        _sprite_animator.SetBool("Bash", false);
    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireCube(_bash_transform.position, box_size);
    }
}
