//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectPlayerHidden : MonoBehaviour {

    [SerializeField] private Transform _player1_transform;
    [SerializeField] private Transform _player2_transform;
    [SerializeField] private Vector3 _offest;
    [SerializeField] private float _max_distance = 40f;

    [SerializeField] private Material _silhouette_material;
    [SerializeField] private float _speed = 4f;
    [SerializeField] private float _max_aplha = 0.75f;
    [SerializeField] private float _delay = 0.75f;
    [SerializeField] private bool _delay_started = false;
    [SerializeField] private bool _delay_complete = false;


    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void FixedUpdate() {
        //Raycast towars the player
        RaycastHit hit_info = new RaycastHit();
        Vector3 direction = ((_player1_transform.position +  _offest) - transform.position).normalized;
        Physics.Raycast(transform.position, direction, out hit_info, _max_distance);

        if (hit_info.collider != null) {
            if (hit_info.collider.tag != "Player" && _silhouette_material.color.a < _max_aplha) {
                if (!_delay_started && !_delay_complete) {
                    StartCoroutine(Delay());
                }
                if (_delay_complete) {
                    Color silhouette_color = _silhouette_material.color;
                    Color additive_color = new Color(0, 0, 0, Time.deltaTime * _speed);
                    _silhouette_material.color = silhouette_color + additive_color;
                }
            }
            else if (hit_info.collider.tag == "Player" && _silhouette_material.color.a > 0) {
                _delay_started = false;
                _delay_complete = false;
                Color sihlouette_color = _silhouette_material.color;
                Color additive_color = new Color(0, 0, 0, Time.deltaTime * _speed);
                _silhouette_material.color = sihlouette_color - additive_color;
            }
        }
    }

    private IEnumerator Delay() {
        _delay_started = true;
        yield return new WaitForSeconds(_delay);
        _delay_complete = true;
    }
}
