//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaCinemachineSwitcher: MonoBehaviour {

    [SerializeField] private Animator _animator;
    [SerializeField] [Tooltip("Animation to area camera")] private string _area_camera_animation_name = "";
    [SerializeField] [Tooltip("Animation to default camera")]private string _default_camera_animation_name = "";
    [SerializeField] private Transform _player_transform;
    //[SerializeField] private float _y_rotation = 0f;
    [SerializeField] private Transform _new_camera_transform;
    [SerializeField] private float _speed = 1f;
    [SerializeField] private bool _reset_rotation = false;
    [SerializeField] private Quaternion _previous_rotation;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (_reset_rotation) {
            _player_transform.rotation = Quaternion.Lerp(_player_transform.rotation, _previous_rotation, Time.deltaTime * _speed);

            if (_player_transform.rotation == _previous_rotation) {
                _reset_rotation = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other) {
        _animator.Play(_area_camera_animation_name);

        //Stores the previous rotation so it can reset it later
        _previous_rotation = _player_transform.rotation;
    }

    private void OnTriggerStay(Collider other) {
        //Target rotation is the new camera transform on the Y axis
        Quaternion target_rotation = Quaternion.Euler(0, _new_camera_transform.eulerAngles.y, 0);
        _player_transform.rotation = Quaternion.Lerp(_player_transform.rotation, target_rotation, Time.deltaTime * _speed);
    }

    private void OnTriggerExit(Collider other) {
        _animator.Play(_default_camera_animation_name);

        //Bool tells the script to reset the player to their previous rotation once they leave the area
        _reset_rotation = true;
    }
}
