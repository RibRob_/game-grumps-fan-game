//Created by RibRob
using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerSceneLoader : MonoBehaviour {

    [SerializeField] private PlayerInformation _player_information;

    private void Awake() {
        Debug.LogWarning("Player Start objects are only meant to be a debug tool. They should not be present in a full build.");
        
        GameObject[] player_starts = GameObject.FindGameObjectsWithTag("Player Start");

        if (Application.isEditor) {

            if (player_starts.Length > 1) {
                Debug.LogException(new Exception("There is more than 1 Player Start object in the scene. Make sure you only have a single Player Start object"));
            }
            else {
                Scene player_scene = SceneManager.GetSceneByName("Player Scene");

                if (!player_scene.isLoaded) {
                    SceneManager.LoadScene("Player Scene", LoadSceneMode.Additive);
                    //SceneManager.SetActiveScene(SceneManager.GetSceneByName("Player Scene"));
                }
            }
        }
    }

    private void Start() {
        if (Application.isEditor && !_player_information.already_loaded) {
            GameObject dan_go = GameObject.Find("Dan");
            GameObject arin_go = GameObject.Find("Arin");

            dan_go.transform.position += transform.position;
            arin_go.transform.position += transform.position;

            _player_information.already_loaded = true;

            Destroy(this.gameObject);
        }
    }

    
    
}
