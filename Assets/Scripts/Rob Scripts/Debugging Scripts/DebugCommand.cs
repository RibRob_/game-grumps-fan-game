//Created by RibRob
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugCommandBase {
    private string _command_id;
    private string _command_description;
    private string _command_format;

    public string CommandID {
        get { return _command_id; }
    }

    public string CommandDescription {
        get { return _command_description; }
    }

    public string CommandFormat {
        get { return _command_format; }
    }

    public DebugCommandBase(string id, string description, string format) {
        _command_id = id;
        _command_description = description;
        _command_format = format;
    }
}
public class DebugCommand : DebugCommandBase {
    private Action command;

    public DebugCommand(string id, string description, string format, Action command) : base(id, description, format) {
        this.command = command;
    }

    public void Invoke() {
        command.Invoke();
    }
}

public class DebugCommand<T1> : DebugCommandBase {
    private Action<T1> command;

    public DebugCommand(string id, string description, string format, Action<T1> command) : base(id, description, format) {
        this.command = command;
    }

    public void Invoke(T1 value) {
        command.Invoke(value);
    }
}
