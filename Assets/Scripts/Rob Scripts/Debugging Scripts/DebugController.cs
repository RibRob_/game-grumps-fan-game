//Created by RibRob
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DebugController : MonoBehaviour {

    [SerializeField] private GameObject _player_gameobject;
    [SerializeField] private Movement _player_movement;
    [SerializeField] private PlayerInformation _player_information;
    [SerializeField] private HUDAnimator _hud_animator;
    [SerializeField] private Bash bash;
    private bool _show_console = false;
    private bool _show_help = true;
    private string _input;

    public List<object> command_list;

    public static DebugCommand CLEAR_ITEMS;
    public static DebugCommand MORE_MONEY;
    public static DebugCommand<int> SET_MONEY;
    public static DebugCommand HELP;
    public static DebugCommand HEAL_DAN;
    public static DebugCommand HEAL_ARIN;
    public static DebugCommand HEAL_BOTH_PLAYERS;
    public static DebugCommand FULL_REFRESH;
    public static DebugCommand TOGGLE_ALIGNMENT_GRID;


    public void Return(InputAction.CallbackContext context) {
        if (_show_console) {
            HandleInput();
            _input = "";
        }
    }

    private void Awake() {
        CLEAR_ITEMS = new DebugCommand("clear_items", "Removes all items from player's inventory.", "clear_items", () => {
            _player_information.ClearItems();
        });

        MORE_MONEY = new DebugCommand("more_money", "Gives the player 100 money.", "more_money", () => {
            _player_information.AddMoney(100);
        });

        SET_MONEY = new DebugCommand<int>("set_money", "Gives the player x anmount of money.", "set_money <money_amount>", (x) => {
            _player_information.AddMoney(x);
        });

        HELP = new DebugCommand("help", "Shows or hides a list of commands", "help", () => {
            _show_help = !_show_help;
        });

        HEAL_DAN = new DebugCommand("heal_dan", "Heals Dan completely", "heal_dan", () => {
            _player_information.HealDan();
        });

        HEAL_ARIN = new DebugCommand("heal_arin", "Heals Arin completely", "heal_arin", () => {
            _player_information.HealDan();
        });

        HEAL_BOTH_PLAYERS = new DebugCommand("heal_both_players", "Heals both players completely", "heal_both_players", () => {
            _player_information.HealBoth();
        });

        FULL_REFRESH = new DebugCommand("full_refresh", "Heals both players completely and refills FP", "full_refresh", () => {
            _player_information.FullRefresh();
        });

        TOGGLE_ALIGNMENT_GRID = new DebugCommand("toggle_alignment_grid", "Turns the HUD alignment grid on and off.", "toggle_alignment_grid", () => {
            _hud_animator.ToggleAlignmentGrid();
        });

        command_list = new List<object> {
            CLEAR_ITEMS,
            MORE_MONEY,
            SET_MONEY,
            HELP,
            HEAL_DAN,
            HEAL_ARIN,
            HEAL_BOTH_PLAYERS,
            FULL_REFRESH,
            TOGGLE_ALIGNMENT_GRID,
        };
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() {
    }

    public void ToggleDebug(InputAction.CallbackContext context) {
        _show_console = !_show_console;
        _player_movement.enabled = !_player_movement.enabled;
        bash.enabled = !bash.enabled;
    }

    Vector2 scroll;

    private void OnGUI() {
        if (_show_console) {

            float y = 0f;

            if (_show_help) {
                GUI.Box(new Rect(0, y, Screen.width, 100), "");

                Rect viewport = new Rect(0, 0, Screen.width - 30, 20 * command_list.Count);

                scroll = GUI.BeginScrollView(new Rect(0, y + 5f, Screen.width, 90), scroll, viewport);

                for (int i = 0; i < command_list.Count; i++) {
                    DebugCommandBase command = command_list[i] as DebugCommandBase;
                    string label = $"{command.CommandFormat} - {command.CommandDescription}";

                    Rect label_rect = new Rect(5, 20 * i, viewport.width - 100, 20);

                    GUI.Label(label_rect, label);

                }

                /*
                 * Semi working code. Doesn't work fully right
                                else
                {
                    for (int i = 0; i < command_list.Count; i++)
                    {

                        if (_input.Length > 0 && _input != null)
                        {
                            for (int j = 0; j < _input.Length; j++)
                            {
                                DebugCommandBase command = command_list[i] as DebugCommandBase;
                                if (command.CommandFormat[j] == _input[j])
                                {
                                    string label = $"{command.CommandFormat} - {command.CommandDescription}";

                                    Rect label_rect = new Rect(5, 20 * i, viewport.width - 100, 20);

                                    GUI.Label(label_rect, label);
                                }
                            }
                        }
                    }
                }
                */

                GUI.EndScrollView();

                y += 100;
            }

            GUI.backgroundColor = new Color(0, 0, 0);
            GUI.Box(new Rect(0, y, Screen.width, 30), "");
            _input = GUI.TextField(new Rect(10f, y + 5f, Screen.width - 20f, 20f), _input);
        }
    }

    private void HandleInput() {

        string[] properties = _input.Split(' ');

        for (int i = 0; i < command_list.Count; i++) {
            DebugCommandBase cb = command_list[i] as DebugCommandBase;

            if (_input.Contains(cb.CommandID)) {

                if (command_list[i] as DebugCommand != null) {
                    (command_list[i] as DebugCommand).Invoke();
                }
                else if (command_list[i] as DebugCommand<int> != null) {
                    (command_list[i] as DebugCommand<int>).Invoke(int.Parse(properties[1]));
                }
            }
        }
    }
}
