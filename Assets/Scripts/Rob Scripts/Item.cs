//Created by RibRob
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "ScriptableObject/Item", order = 3)]
public class Item : ScriptableObject {

    public string item_name = "Empty";
    public string description = "Empty";
    public Sprite sprite;
    public int value = 0;
}
